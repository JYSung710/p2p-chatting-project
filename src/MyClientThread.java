import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/** Client thread
 * 
 * sends data to server thread
 * @author JYSung
 * 
 */
public class MyClientThread extends Thread{
	private int remoteport;
	private InetAddress remoteaddr;
	PrintWriter pw;
	static Socket socket;
	
	public MyClientThread(String ip, int port) {
		this.remoteport = port;
		try {
			this.remoteaddr = InetAddress.getByName(ip);
		} //set remote address and port, instantiate socket
		
		catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		try {
			socket = new Socket(this.remoteaddr, this.remoteport);
			Main.sendContents.setEditable(true);
			Main.sendButton.setEnabled(true);
			Main.chattingLog.append("Connected to "+remoteaddr+":"+remoteport+"\n");
			
			this.pw = new PrintWriter(socket.getOutputStream(), true); // auto flushing print writer
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
