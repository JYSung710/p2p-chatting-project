import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/** Server thread
 * 
 * receives data from client thread
 * @author JYSung
 *
 */
public class MyServerThread extends Thread {
	static ServerSocket servSocket;
	static Socket rcvSocket;
	static DatagramPacket rcv_packet;// 수신용 데이터그램 패킷 
	BufferedReader input;
	private String ipAddress;
	private int remoteport;
	
	public MyServerThread(int port) {
		try {
			this.servSocket = new ServerSocket(port);
			Main.serverPort.setEditable(false);
			Main.listenButton.setEnabled(false);
			Main.chattingLog.append("Server set to port : " +servSocket.getLocalPort()+"\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void run() {
		try {
			rcvSocket = servSocket.accept();
			input = new BufferedReader(new InputStreamReader(rcvSocket.getInputStream()));
			
			String msg;
			while(true){
				msg = input.readLine();
				if(msg == null) continue;
				Main.chattingLog.append("You : "+msg+"\n");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
