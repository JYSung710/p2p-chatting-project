import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import java.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.ScrollPaneConstants;

public class Main {

	private JFrame frame;
	
	public static CustomTextField friendsIP;
	public static CustomTextField friendsPort;
	public static CustomTextField serverPort;
	static CustomTextField sendContents;
	
	public static JTextArea chattingLog;
	public static MyClientThread mct;
	public static MyServerThread mst;

	static JButton listenButton;
	static JButton sendButton;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Computer Network P2P Project");
		frame.setBounds(100, 100, 509, 578);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		serverPort = new CustomTextField();
		serverPort.setPlaceholder("Input port");
		serverPort.setBounds(20, 19, 130, 24);
		frame.getContentPane().add(serverPort);
		serverPort.setColumns(10);
		
		listenButton = new JButton("Listen");
		listenButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int port = Integer.parseInt(serverPort.getText());

				// Generate my TCP server thread 
				mst = new MyServerThread(port);
				mst.start();
			}
		});
		listenButton.setBounds(162, 19, 117, 27);
		frame.getContentPane().add(listenButton);
		
		
		friendsIP = new CustomTextField();
		friendsIP.setPlaceholder("Friend's IP");
		friendsIP.setBounds(20, 55, 246, 24);
		frame.getContentPane().add(friendsIP);
		friendsIP.setColumns(10);
		
		friendsPort = new CustomTextField();
		friendsPort.setPlaceholder("Port");
		friendsPort.setBounds(278, 55, 86, 24);
		frame.getContentPane().add(friendsPort);
		friendsPort.setColumns(10);
		
		JButton connectButton = new JButton("Connect");
		
		//After clicking 'Connect' button 
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String ip = friendsIP.getText();
				int port = Integer.parseInt(friendsPort.getText());
				
				// Generate a TCP Client Thread to connect with friend's TCP Server
				 mct = new MyClientThread(ip,port);
				 mct.start();
				
				
				
			}
		});
		
		
		connectButton.setBounds(376, 55, 113, 27);
		frame.getContentPane().add(connectButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(20, 100, 469, 390);
		frame.getContentPane().add(scrollPane);
		
		chattingLog = new JTextArea();
		chattingLog.setEditable(false);
		chattingLog.setLineWrap(true);
		scrollPane.setViewportView(chattingLog);
		
		sendContents = new CustomTextField();
		sendContents.setEditable(false);
		sendContents.setPlaceholder("Input message");
		sendContents.setColumns(10);
		sendContents.setBounds(20, 513, 354, 24);
		frame.getContentPane().add(sendContents);
		
		sendButton = new JButton("Send");
		sendButton.setEnabled(false);
		
		//After clicking 'Send' button 
		sendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String chattingContents = sendContents.getText();
				
				// Send the chatting contents to friend's Server by my Client Thread
				mct.pw.println(chattingContents);
				chattingLog.append("Me: "+chattingContents+"\n");
				sendContents.setText("");
				
			}
		});		
		sendButton.setBounds(376, 513, 113, 27);
		frame.getContentPane().add(sendButton);
		frame.getRootPane().setDefaultButton(sendButton);
	}
}
